import React, { useState } from 'react';
import { useQuery } from '@apollo/react-hooks';

import { getBooksQuery } from '../queries/queries';

import BookDetails from './BookDetails';

export interface AllBooks {
  books: Book[]
}

export interface Book {
  book: any;
  cb: any;
}

const BookList:React.FC = () => {
  const { loading, data } = useQuery(
    getBooksQuery
  );
  const [selectedBook, setSelectedBook] = useState();

  if (data && data.books) {
    data.books = data.books.map((b:any) => {
      b.cb = setSelectedBook;
      return b;
    });
  }

  return (
    <div>
      <ul className="book-list">
        {loading ? renderLoading() : null}
        {data && data.books && !loading ? data.books.map(renderBooks) : null}
      </ul>
      <BookDetails id={selectedBook}/>
    </div>
  );
};

function renderBooks(book:any, i:number) {
  return <li key={i} onClick={(e) => book.cb(book.id)}>{`${book.name}`}</li>
}

function renderLoading() {
  return <p>Loading...</p>
}

export default BookList;
