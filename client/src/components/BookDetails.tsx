import React from 'react';
import { useQuery } from 'react-apollo';

import { getBookQuery } from '../queries/queries';

export interface BookDetailProps {
  id?: string;
}

const BookDetails:React.FC<BookDetailProps> = (props) => {
  const { id } = props;
  const { loading, data } = useQuery(getBookQuery, {
    variables: {
      id: id
    }
  });
  const book = data && data.book && !loading ? data.book : null;

  return (
    <div>
      {book ? renderBook(book) : null}
    </div>
  );
};

function renderBook (book:any) {
  return (
    <div className="book-details">
      <h2>{book.name}</h2>
      <p>{book.genre}</p>
      <p>{book.author.name}</p>
      <p>All Books by this auhtor</p>
      <ul>
        {book.author.books.map((e:any, i:number) => {
          return <li key={i}>{e.name}</li>
        })}
      </ul>
    </div>
  );
} 

export default BookDetails;
