import React, { useState } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';

import { getAuthorsQuery, addBookMutation, getBooksQuery } from '../queries/queries';

export interface AllAuthors {
  authors: Author[]
}

export interface Author {
  author: any;
}

const AddBook:React.FC = () => {
  const {loading, data} = useQuery<AllAuthors, Author>(
    getAuthorsQuery
  );
  const [addBook] = useMutation(addBookMutation);
  const [bookName, setBookName] = useState('');
  const [bookGenre, setBookGenre] = useState('');
  const [authorId, setAuthorId] = useState('');

  return (
    <form onSubmit={((e) => {
      e.preventDefault();
      addBook(
        {
          variables: {
            name:bookName,
            genre: bookGenre,
            authorId:authorId
          },
          refetchQueries: [
            {
              query: getBooksQuery
            }
          ]
        }
      );
    })}>
      <div className="field">
        <label htmlFor="book-name">Book Name</label>
        <input
          type="text"
          id="book-name"
          onChange={(e) => setBookName(e.target.value)}
        />
      </div>

      <div className="field">
        <label htmlFor="book-genre">Genre</label>
        <input
          type="text"
          id="book-genre"
          onChange={(e) => {setBookGenre(e.target.value)}}
        />
      </div>

      <div className="field">
        <label htmlFor="author">Author</label>
        <select name="author" id="author" onChange={(e) => setAuthorId(e.target.value)}>
          <option value="">Select Author</option>
          {loading ? <option disabled>Loading Authors...</option> : null}
          {data && data.authors ? data.authors.map(renderAuthors) : null}
        </select>
      </div>

      <button>+</button>
    </form>
  );
};

function renderAuthors(author:any, i:number) {
  return <option value={author.id} key={i}>{author.name}</option>
}

export default AddBook;
