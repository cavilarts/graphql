import React from 'react';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';

// Components
import BookList from './components/BookList';
import AddBook from "./components/AddBook";

import './App.css';

// Apollo setup
const client = new ApolloClient({
  uri: 'http://localhost:3001/graphql'
});

function App() {
  return (
    <section className="main">
      <ApolloProvider client={client}>
        <h1>Reading list</h1>
        <BookList />
        <AddBook />
      </ApolloProvider>
    </section>
  );
}

export default App;
